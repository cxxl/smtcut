
import sys, os
exedir = os.path.dirname(__file__)
sys.path.append(os.path.join(exedir, ".."))
from smtcut import Config, compute_dnf_from_input, load_ph_from_multi_lp, smt_compare

# test different tropicalization options
# test preprocessing
# test all solvers
# build suite of models

# fix doctest code
# check preprocess loop

import os


def run_test():
    cfg = Config()
    cfg.compare = True
    cfg.dbdir = exedir
    cfg.verbose = 0
    cfg.save_log = False
    cfg.save_solution = False
    cfg.bench_logs = True

    names = [
        # regular models
        "BIOMD0000000026", "BIOMD0000000028", "BIOMD0000000030",
        # models with a zero line
        "BIOMD0000000005",      # has zero parameters, even in denom!
        "BIOMD0000000035",
        # rational functions
        "BIOMD0000000004",
    ]

    for solver in ["msat", "z3"]:
        print(f"\nSolver: {solver}")
        cfg.solver_name = solver

        for basename in names:
            cfg.mangler.clear()

            print(end=basename)
            ret = compute_dnf_from_input(cfg, basename, "")
            print(end=f": trop {ret.tropicalization_time:.03f} sec, dnf {ret.total_time:.03f} sec")
            print(end=f"{'' if ret.compare else ' **ERROR**'}")

            corr = load_ph_from_multi_lp(os.path.join(cfg.dbdir, basename, "ep11-solutions.txt"))
            r = smt_compare(cfg, [corr], [ret.solution])
            print(f"{' **WRONG**' if not r else '' if len(corr) == len(ret.solution) else ' **NOT MINIMAL**'}")


if __name__ == "__main__":
    run_test()
