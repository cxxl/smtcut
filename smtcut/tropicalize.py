#! /usr/bin/env python3

# $+HEADER$
#
# Copyright 2019-2021 Christoph Lueders
#
# This file is part of the SMTcut project: <http://wrogn.com/smtcut>
#
# SMTcut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SMTcut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with SMTcut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

# is imported by: smtcut

# pylance local imports issue:
# https://github.com/microsoft/pylance-release/issues/68

import sys, os, re
from sympy import sympify, Rational, sstr, gcd, Symbol, lcm_list, Mul
from math import log
from operator import mod
from gmpy2 import mpq
from pprint import pprint

from .util import mytime
from .phkeep import Bag, PhKeep, mpq_str
from .config import Config


def nstr(f):
    return sstr(f, order="none")


class CollectProd:
    """
    Collect a product of parameters (and maybe a literal factor).
    """
    def __init__(self, log):
        self.prod = 1
        self.log = log
    def __call__(self, x=None, pow=1):
        if x is not None:
            self.prod *= x ** pow
    def get(self):
        return self.log(self.prod)

class CollectSum:
    """
    Collect a sum of rounded logarithms of parameters (and maybe a literal factor).
    """
    def __init__(self, log):
        self.sum = 0
        self.log = log
    def __call__(self, x=None, pow=1):
        if x is not None:
            self.sum += pow * self.log(x)
    def get(self):
        return self.sum


def make_mpq(i):
    """
    Convert to gmpy2.mpq.  Accepts int, Rational

    >>> make_mpq(-2)
    mpq(-2,1)
    >>> make_mpq(Rational(2,3))
    mpq(2,3)
    >>> make_mpq(0.25)
    mpq(1,4)
    """
    try:
        # int, float, string
        return mpq(i)
    except TypeError:
        # Rational
        return mpq(i.numerator(), i.denominator())


def _gcd_tuple(d):
    g = make_mpq(gcd(d))
    if g == 0:
        return tuple(d)
    if g < 0:
        # must not change sign
        g = -g
    return tuple(i / g for i in d)


split_pattern = re.compile(r"\A(?:([a-zA-Z]+)_?([0-9]+)|([a-zA-Z]+))\Z")
inf = float("inf")

def split_name_num(s):
    """
    Split a variable name into "base name" and index.
    Index will be -inf, if no index was given.

    >>> split_name_num("x1")
    ('x', 1)
    >>> split_name_num("x_1")
    ('x', 1)
    >>> split_name_num("x0")
    ('x', 0)
    >>> split_name_num("x10")
    ('x', 10)
    >>> split_name_num(sympify("x10"))
    ('x', 10)
    >>> split_name_num("x")
    ('x', -inf)
    >>> split_name_num("$") is None
    True
    >>> split_name_num("x") < split_name_num("x0")
    True
    """
    s = str(s)
    m = split_pattern.match(s)
    if not m:
        return None
    if m.group(3):
        return m.group(3), -inf
    return m.group(1), int(m.group(2))


def logep(gamma, eps, p=Rational(1)):
    """
    Compute a c from gamma's absolute value.

    >>> logep(2, Rational(1,2), 1)
    -1
    >>> logep(Rational("1.3"), Rational(1,2), 10)
    -2/5
    >>> logep(Rational("1.3"), Rational(1,2), 100)
    -19/50
    """
    assert p == int(p) and p > 0
    p = Rational(p)
    assert gamma != 0
    return round(p * log(gamma * (-1 if gamma < 0 else 1), eps)) / p


class Tropicalize:
    def __init__(self, cfg, *, evaluate_literals=False, round_once=False, substitute_first=False, param={}, vars=[]):
        """
        logep: log function with round and scale to base epsilon in (0,1).
        param: dict with parameter names (str) and values (number).
        vars: a list of variables (str) that specifies their order.
        """
        self.cfg = cfg
        self.eps = cfg.eps
        self.p = cfg.p
        self.prt = cfg.prt
        self.verbose = cfg.verbose
        self.evaluate_literals = evaluate_literals
        self.round_once = round_once
        self.substitute_first = substitute_first
        self.param = param
        self.eqns = []
        self.vars = vars

    def _load_parameters(self, seq, fn):
        """
        >>> cfg = Config()
        >>> t = Tropicalize(cfg)
        >>> seq = ["k1 = 12", "k2=1.5", " k3 =  -12/13", "k4 = -1e-75"]
        >>> t._load_parameters(seq, "-")
        {'k1': 12, 'k2': 3/2, 'k3': -12/13, 'k4': -1/1000000000000000000000000000000000000000000000000000000000000000000000000000}
        """
        d = {}
        line = 0

        for ln in seq:
            line += 1
            ln = ln.strip()
            # ignore empty or comment lines
            if ln[:1] in ("", ";", "#"):
                continue
            a = ln.split("=")
            if len(a) != 2:
                self.prt(f"{fn} ({line}): parameter lines must be: VARIABLE = VALUE\n", screen=self.verbose >= 0)
                continue
            d[a[0].strip()] = Rational(a[1].strip())

        return d

    def load_parameters(self, fn):
        """
        Read file 'fn' as key=value pairs and save a dict with str:Rational pairs as self.param.
        """
        self.prt(f"Loading parameters from {fn} ...", screen=self.verbose >= 1)
        with open(fn) as seq:
            self.param = self._load_parameters(seq, fn)

    def _load_equations(self, seq):
        """
        >>> cfg = Config()
        >>> t = Tropicalize(cfg)
        >>> seq = ["k2*x1 + 2*x2 - x1*x2", "x2**k1 - log(3)*x3", "x + z"]
        >>> print(nstr(t._load_equations(seq)))
        ([k2*x1 + 2*x2 - x1*x2, x2**k1 - log(3)*x3, x + z], [k1, k2, x, x1, x2, x3, z])
        """
        eqns = []
        line = 0
        vars = set()

        for ln in seq:
            ln = ln.strip()
            # ignore empty or comment lines
            if ln[:1] in ("", ";", "#"):
                continue
            f = sympify(ln, evaluate=False)
            eqns.append(f)
            vars.update(str(i) for i in f.free_symbols)

        # build list of variables, filter out parameter names and sort properly
        vars = sorted(vars - set(self.param.keys()), key=lambda x: split_name_num(x))
        return eqns, vars

    def load_equations(self, fns):
        """
        Read file 'fns' as r.h.s. of 0 = f and
        return a list of sympy expressions and a sorted list of variables.
        Also accepts list of filenames, which are then read one after the other to form one set of input equations.
        """
        l = []
        if not any(isinstance(fns, i) for i in (list, tuple)):
            fns = [fns]
        for fn in fns:
            self.prt(f"Loading {fn} ...", screen=self.verbose >= 1)
            with open(fn) as seq:
                l.extend(seq.readlines())
        self.eqns, self.vars = self._load_equations(l)

    @staticmethod
    def T_trop_term(s, **kwargs):
        """
        >>> print(Tropicalize.T_trop_term("x**2+2*y-7*z**5", evaluate_literals=True, vars=["x", "y", "z"]))
        ([(0, 2, 0, 0), (1, 0, 1, 0)], [(3, 0, 0, 5)])

        >>> print(Tropicalize.T_trop_term("x**2+k1*y-k2*z**5", param={"k1":2, "k2":7}, vars=["x", "y", "z"]))
        ([(0, 2, 0, 0), (1, 0, 1, 0)], [(3, 0, 0, 5)])

        >>> print(Tropicalize.T_trop_term("x**2+k1*y-k2*y", param={"k1":2, "k2":2}, vars=["x", "y"]))
        ([(0, 2, 0), (1, 0, 1)], [(1, 0, 1)])

        >>> print(Tropicalize.T_trop_term("x**2+k1*y-k2*y", evaluate_literals=True, substitute_first=True, param={"k1":2, "k2":2}, vars=["x", "y"]))
        <BLANKLINE>
        *** Equation 0 has unbalanced monomials, ignoring. None
        """
        cfg = Config()
        cfg.eps = 2
        t = Tropicalize(cfg, **kwargs)
        t.verbose = 2
        r = t.trop_term(sympify(s))
        if r is not None:
            pp, np = r
            return mpq_str((sorted(pp), sorted(np)))
        else:
            return None

    def trop_term(self, f, eqcnt=0):
        """
        Tropicalize equation f = 0, where f is a sympy expression.
        Returns (positive points, negative points), where the coordinates are of type 'mpq'.
        """
        dim = len(self.vars)
        assert len(set(self.vars)) == dim                        # no double use
        assert not (set(self.vars) & set(self.param.keys()))          # must be disjunct
        assert not self.substitute_first or self.evaluate_literals    # substitute_first requires not evaluate_literals
        assert set(str(i) for i in f.free_symbols) <= set(self.vars) | set(self.param.keys())  # no unknown symbols

        # evaluate to get rid of log(2) and the like
        if self.verbose >= 3:
            self.prt(f"\nInput:    {f}")
        if self.substitute_first:
            f = f.subs(self.param, evaluate=False)
        f = f.evalf().expand()
        if self.verbose >= 3:
            self.prt(f"Expanded: {f}")
        if f == 0:
            self.prt(end=f"\n*** Equation {eqcnt} is zero, ignoring. ", screen=self.verbose >= 2)
            return None
        tt, v = f.as_terms()

        # check for denominators
        denoms = []
        for _,((re,im),monom,ncpart) in tt:
            for i,m in enumerate(monom):
                # only consider denominators that contain variables
                if m < 0 and type(v[i]) != Symbol and set(str(i) for i in v[i].free_symbols) - set(self.param.keys()):
                    # denominator found
                    lfact = []
                    for fact in v[i].factor().as_ordered_factors():
                        if set(str(i) for i in fact.free_symbols) - set(self.param.keys()):
                            # this factor contains variables
                            lfact.append(fact)
                    if lfact:
                        denoms.append(Mul(*lfact) ** -m)

        if denoms:
            # multiply equation with lcm of all denoms
            denom = lcm_list(denoms).factor()
            self.prt(end=f"\n*** Multiplying equation {eqcnt} with {denom}. ", screen=self.verbose >= 1)
            f = (f * denom).cancel().expand()
            if self.verbose >= 3:
                self.prt(f"Canceled: {f}")
            tt, v = f.as_terms()

        assert all(type(i) == Symbol for i in v)            # all 'variables' in 'v' must be symbols
        v = [str(i) for i in v]                             # convert to str
        assert set(v) <= set(self.vars) | set(self.param.keys())  # must be listed in self.vars

        # build translation list between self.vars and v (mm), and
        # list of parameter values (pv)
        mm = [-1] * len(v)
        pv = [0] * len(v)
        for i,s in enumerate(v):
            try:
                mm[i] = self.vars.index(str(s)) + 1
            except ValueError:
                pv[i] = self.param[str(s)]

        # set of positive and negative points
        pp, np = set(), set()
        mylog = lambda x: logep(x, self.eps, self.p)

        # cycle through all monomials
        for term,((re,im),monom,ncpart) in tt:
            # each tuple is (term, ((real, imag), monom, ncpart))
            assert re != 0
            assert im == 0
            assert not ncpart
            # print(f"term={_}")

            sign = 1 if re > 0 else -1
            # either add the rounded logs, or multiply the values and do the rounded log once at the end
            collect = (CollectProd if self.round_once else CollectSum)(mylog)
            p = [0] * (1 + dim)
            if self.evaluate_literals:
                # in tropicalization, usually literals are ignored: i.e., Trop(2*x) = Trop(x)
                collect(sign * re)
            #print(monom, v)
            for i,m in enumerate(monom):
                idx = mm[i]
                if idx == -1:
                    # parameter
                    assert not self.substitute_first        # can not happen otherwise
                    # print(m, pv[i])
                    if pv[i] == 0:
                        if m != 0:
                            self.prt(end=f"\n*** Monomial {term} in equation {eqcnt} is zero, since {v[i]}=0. ", screen=self.verbose >= 2)
                            sign = 0
                            break
                    else:
                        collect(pv[i], m)
                else:
                    # variable
                    p[idx] = m
            # save the value of the rounded log collection
            p[0] = collect.get()

            # add point to the right set
            if sign != 0:
                (pp if sign == 1 else np).add(tuple(make_mpq(i) for i in p))

        if not pp and not np:
            self.prt(end=f"\n*** Equation {eqcnt} has no monomials, ignoring. ", screen=self.verbose >= 1)
            return None

        if not pp or not np:
            self.prt(end=f"\n*** Equation {eqcnt} has unbalanced monomials, ignoring. ", screen=self.verbose >= 1)
            return None

        return pp, np

    def build_polyhedra(self, pp, np):
        b = Bag()
        from itertools import product

        # prt(end=f"{len(pp),len(np)}", flush=True)
        for v1, v2 in product(pp, np):
            # v1 = v2, hence v1 - v2 = 0.  This defines a hyperplane.
            d = _gcd_tuple([a - b for a, b in zip(v1, v2)])
            # Furthermore, v1 = v2 <= vi (where vi \in pp \cup np),
            # hence vi - v1 >= 0.  This defines the half-spaces.
            ie = []
            for vi in set(pp) | set(np):
                if vi != v1 and vi != v2:
                    ie.append(_gcd_tuple([a - b for a, b in zip(vi, v1)]))
            # create the polyhedron
            p = PhKeep(eqns=[d], ieqs=ie)
            if not p.is_empty(self.cfg):
                b.insert_include(self.cfg, p.minimize(self.cfg))

        return b

    def tropicalize(self, param_fn, eqn_fns, ret=None):
        """
        From 'dir' load equations and tropicalize them with epsilon='self.eps' and p='self.p'.
        """
        self.load_parameters(param_fn)
        # print(self.param)
        self.load_equations(eqn_fns)
        # pprint(self.eqns)

        self.prt(end=f"Tropicalizing (eps={self.eps}, p={self.p}) ... ", screen=self.verbose >= 1)
        t = mytime()
        bb = []
        for cnt, f in enumerate(self.eqns, 1):
            self.prt(end=f"[{cnt}]", flush=True, screen=self.verbose >= 1)
            r = self.trop_term(f, cnt)
            if r is not None:
                b = self.build_polyhedra(*r)
                bb.append(b)
        t = mytime() - t
        self.prt(f"\nTropicalization {t:.3f} sec\n", flushfile=True, screen=self.verbose >= 1)

        if ret:
            ret.tropicalization_time = t

        return bb


def tester():
    from .prt import prt
    prt("test: tropicalize")
    import doctest, sys
    this_mod = sys.modules[__name__]
    doctest.testmod(this_mod, verbose=False)
